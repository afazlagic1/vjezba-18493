package ba.unsa.etf.rma.vj_18493.list;

public interface IMovieListPresenter {
    void searchMovies(String query);
    void refreshMovies();
}
