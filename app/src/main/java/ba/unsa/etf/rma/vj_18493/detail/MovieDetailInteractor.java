package ba.unsa.etf.rma.vj_18493.detail;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18493.data.Movie;

class MovieDetailInteractor extends AsyncTask<String, Integer, Void> {
    private Movie movieFound;

    @Override
    protected Void doInBackground(String... strings) {
        ArrayList<JSONArray> genre_ids = new ArrayList<>();
        String query = null;
        try {
            query = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String tmdb_api_key = "2a2647101344323e2a66b5b15a0209f6";
        String url1 = "https://api.themoviedb.org/3/movie/"+ strings[0] +"?api_key=" + tmdb_api_key + "&language=en-US";
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection)
                    url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject movie = new JSONObject(rezultat);
                String title = movie.getString("title");
                Integer id = movie.getInt("id");
                String posterPath = movie.getString("poster_path");
                String overview = movie.getString("overview");
                String releaseDate = movie.getString("release_date");
            movieFound = new Movie(id,title,overview,releaseDate,posterPath);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    public interface onMovieFound {
        void onFound(Movie movie);
    }

    private onMovieFound onMovieFound;

    public MovieDetailInteractor(onMovieFound onMovieFound) {
        this.onMovieFound = onMovieFound;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        onMovieFound.onFound(movieFound);
    }
}
