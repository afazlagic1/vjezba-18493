package ba.unsa.etf.rma.vj_18493.detail;

import ba.unsa.etf.rma.vj_18493.data.Movie;

public interface IMovieDetailView {
    void setMovie(Movie movie);
}
