package ba.unsa.etf.rma.vj_18493.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Movie implements Parcelable {
    private String title = "";
    private String genre = "";
    private String releaseDate = "12";
    private String homepage = "";
    private String overview = "";
    private List<String> actors = new ArrayList<>();
    private List<String> similarMovies = new ArrayList<>();
    private int id;
    private String posterPath;

    public List<String> getSimilarMovies() {
        return similarMovies;
    }

    public void setSimilarMovies(ArrayList<String> similarMovies) {
        this.similarMovies = similarMovies;
    }

    protected Movie(Parcel parcel) {
        this.title = parcel.readString();
        this.overview = parcel.readString();
        this.releaseDate = parcel.readString();
        this.homepage = parcel.readString();
        this.genre = parcel.readString();
        this.actors = parcel.createStringArrayList();
        this.id = parcel.readInt();
        this.posterPath = parcel.readString();
        this.similarMovies = parcel.createStringArrayList();
    }
    public Movie(String title, String overview, String releaseDate, String homepage, String genre, List<String> actors, int id, String posterPath) {
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.overview = overview;
        this.actors = actors;
        this.id = id;
        this.posterPath = posterPath;
    }

    public Movie(Integer id, String title, String overview, String releaseDate, String posterPath) {
        this.title = title;
        this.releaseDate = releaseDate;
        this.overview = overview;
        this.id = id;
        this.posterPath = posterPath;
    }

    public Movie(String title, String overview, String releaseDate, String homepage, String genre, ArrayList<String> actors, ArrayList<String> similarMovies, int id, String posterPath) {
        this.title = title;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.genre = genre;
        this.actors=actors;
        this.similarMovies = similarMovies;
        this.id = id;
        this.posterPath = posterPath;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getHomepage() {
        return homepage;
    }

    public String getOverview() {
        return overview;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.genre);
        dest.writeString(this.releaseDate);
        dest.writeString(this.homepage);
        dest.writeString(this.overview);
        dest.writeStringList(this.actors);
        dest.writeStringList(this.similarMovies);
        dest.writeInt(this.id);
        dest.writeString(this.posterPath);
    }

    public static final Parcelable.Creator<Movie> CREATOR
            = new Parcelable.Creator<Movie>() {
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}