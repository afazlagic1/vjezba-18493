package ba.unsa.etf.rma.vj_18493.list;

import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18493.data.Movie;

public class MovieListInteractor extends AsyncTask<String, Integer, Void> implements IMovieListInteractor {

    private ArrayList<Movie> movies = new ArrayList<>();

    public interface OnMoviesSearchDone{
        void onDone(ArrayList<Movie> results);
    }
    private OnMoviesSearchDone caller;
    public MovieListInteractor(OnMoviesSearchDone p) {
        caller = p;
    };

    @Override
    protected Void doInBackground(String... strings) {
        ArrayList<JSONArray> genre_ids = new ArrayList<>();
        String query = null;
        try {
            query = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String tmdb_api_key = "2a2647101344323e2a66b5b15a0209f6";
        String url1 = "https://api.themoviedb.org/3/search/movie?api_key="
                +tmdb_api_key+"&query=" + query;
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection)
                    url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray results = jo.getJSONArray("results");
            for (int i = 0; i < 5; i++) {
                JSONObject movie = results.getJSONObject(i);
                String title = movie.getString("title");
                Integer id = movie.getInt("id");
                String posterPath = movie.getString("poster_path");
                String overview = movie.getString("overview");
                String releaseDate = movie.getString("release_date");
                genre_ids.add(movie.getJSONArray("genre_ids"));
                //Filmove dodajemo u listu koja je privatni atribut klase
                movies.add(new Movie(id,title,overview,releaseDate,posterPath));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url2 = "https://api.themoviedb.org/3/genre/movie/list?api_key=" + tmdb_api_key + "&language=en-US";
        try {
            URL url = new URL(url2);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray results = jo.getJSONArray("genres");
            for(int j = 0; j < movies.size(); j++) {
                for (int i = 0; i < results.length(); i++) {
                    JSONObject genreForMovie = results.getJSONObject(i);
                    Integer idGenre = genreForMovie.getInt("id");
                    String name = genreForMovie.getString("name");
                    for(int k = 0; k < genre_ids.get(j).length(); k++)
                    if(idGenre.equals(genre_ids.get(j).getInt(k))) {
                        movies.get(j).setGenre(name);
                        break;
                        }
                    }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

        private String convertStreamToString(InputStream in) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            return sb.toString();
        }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        caller.onDone(movies);
    }

    @Override
    public ArrayList<Movie> getMovies() {
        return movies;
    }
}
