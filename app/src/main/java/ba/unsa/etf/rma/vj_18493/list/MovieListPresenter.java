package ba.unsa.etf.rma.vj_18493.list;

import android.content.Context;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18493.data.Movie;

public class MovieListPresenter implements IMovieListPresenter, MovieListInteractor.OnMoviesSearchDone {
    private Context context;
    private IMovieListView view;

    @Override
    public void refreshMovies() {
        //view.setMovies(movieListInteractor.getMovies());
        //view.notifyMovieListDataChanged();
    }

    public MovieListPresenter(IMovieListView view, Context context) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void onDone(ArrayList<Movie> results) {
        view.setMovies(results);
        view.notifyMovieListDataChanged();
    }

    @Override
    public void searchMovies(String query) {
        new MovieListInteractor(this).execute(query);
    }
}
