package ba.unsa.etf.rma.vj_18493.detail;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.lang.reflect.Field;
import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18493.MovieDBOpenHelper;
import ba.unsa.etf.rma.vj_18493.R;
import ba.unsa.etf.rma.vj_18493.data.Movie;

import static ba.unsa.etf.rma.vj_18493.MovieDBOpenHelper.MOVIE_GENRE;
import static ba.unsa.etf.rma.vj_18493.MovieDBOpenHelper.MOVIE_ID;
import static ba.unsa.etf.rma.vj_18493.MovieDBOpenHelper.MOVIE_TITLE;

public class MovieDetailFragment extends Fragment implements IMovieDetailView {

    private TextView title;
    private TextView genre;
    private TextView overview;
    private TextView releaseDate;
    private TextView homepage;
    private ListView actors;
    private Button share;
    private ToggleButton toggleButton;
    private ImageView imageView;

    private IMovieDetailPresenter presenter;

    public IMovieDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = (IMovieDetailPresenter) new MovieDetailPresenter(this, getActivity());
        }
        return presenter;
    }
    private View.OnClickListener homepageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String url = getPresenter().getMovie().getHomepage();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private View.OnClickListener titleOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_SEARCH);
            intent.setPackage("com.google.android.youtube");
            intent.putExtra("query", getPresenter().getMovie().getTitle() + " trailer");
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private View.OnClickListener shareOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, getPresenter().getMovie().getOverview());
            intent.setType("text/plain");
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener toggleOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                Bundle arguments = new Bundle();
                arguments.putStringArrayList("similar", (ArrayList<String>) getPresenter().getMovie().getSimilarMovies());
                SimilarFragment similarFragment = new SimilarFragment();
                similarFragment.setArguments(arguments);
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.frame, similarFragment).commit();
            } else {
                Bundle arguments = new Bundle();
                arguments.putStringArrayList("cast", (ArrayList<String>) getPresenter().getMovie().getActors());
                CastFragment castFragment = new CastFragment();
                castFragment.setArguments(arguments);
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.frame, castFragment).commit();
            }
        }
    };
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        if (getArguments() != null && getArguments().containsKey("movieid")) {
            getPresenter().setID(getArguments().getInt("movieid"));
            title = view.findViewById(R.id.title);
            genre = view.findViewById(R.id.genre);
            overview = view.findViewById(R.id.overview);
            releaseDate = view.findViewById(R.id.releaseDate);
            homepage = view.findViewById(R.id.homepage);
            share = view.findViewById(R.id.share);
            toggleButton = view.findViewById(R.id.toggle_button);
            imageView = view.findViewById(R.id.icon);
            /*getPresenter().setMovie(getArguments().getParcelable("movie"));
            Movie movie = getPresenter().getMovie();
            String genreMatch = movie.getGenre();
            try {
                Class res = R.drawable.class;
                Field field = res.getField(genreMatch);
                int drawableId = field.getInt(null);
                imageView.setImageResource(drawableId);
            } catch (Exception e) {
                imageView.setImageResource(R.drawable.ic_launcher_background);
            } */

            homepage.setOnClickListener(homepageOnClickListener);
            title.setOnClickListener(titleOnClickListener);
            share.setOnClickListener(shareOnClickListener);
            toggleButton.setOnCheckedChangeListener(toggleOnCheckedChangeListener);
        }
        return  view;
    }

    @Override
    public void setMovie(Movie movie) {
        title.setText(movie.getTitle());
        genre.setText(movie.getGenre());
        overview.setText(movie.getOverview());
        releaseDate.setText(movie.getReleaseDate());
        homepage.setText(movie.getHomepage());
        String posterPath = "https://image.tmdb.org/t/p/w342";
        Glide.with(getContext())
                .load(posterPath+movie.getPosterPath())
                .centerCrop()
                .placeholder(R.drawable.movie)
                .error(R.drawable.movie)
                .fallback(R.drawable.movie)
                .into(imageView);
        Bundle arguments = new Bundle();
        arguments.putStringArrayList("cast", (ArrayList<String>) movie.getActors());
        CastFragment castFragment = new CastFragment();
        castFragment.setArguments(arguments);
        getChildFragmentManager().beginTransaction()
                .add(R.id.frame, castFragment)
                .commit();

        String[] koloneRezulat = new String[]{ MOVIE_ID, MOVIE_TITLE, MOVIE_GENRE};
        ContentValues values = new ContentValues();
        values.put(MOVIE_TITLE,movie.getTitle());
        values.put(MOVIE_GENRE,movie.getGenre());
        values.put(MOVIE_ID, movie.getId());
        MovieDBOpenHelper movieDBOpenHelper = new MovieDBOpenHelper(getContext());
        SQLiteDatabase db = movieDBOpenHelper.getWritableDatabase();
        db.insert(MovieDBOpenHelper.MOVIE_TABLE,null,values);
    }
}