package ba.unsa.etf.rma.vj_18493.list;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18493.R;
import ba.unsa.etf.rma.vj_18493.data.Movie;

public class MovieListAdapter extends ArrayAdapter<Movie> {
    int resource;
    Context context;
    private String posterPath="https://image.tmdb.org/t/p/w342";

    public MovieListAdapter(Context _context, int _resource, ArrayList<Movie> items) {
        super(_context, _resource, items);
        resource  = _resource;
        context = _context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        /* Metoda getView sluzi za kreiranje, inflate (proces kreiranja UI iz XMLa) i popunjavanje View objekta koji ce biti dodan roditeljskom Adapter
         View-u */
        LinearLayout newView;
        if(convertView == null) { //tek kreiran view
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater) getContext().getSystemService(inflater);
            li.inflate(resource, newView, true);
        }
        else { //update samo
            newView = (LinearLayout) convertView;
        }
        Movie objekat = getItem(position);

        TextView itemName = (TextView) newView.findViewById(R.id.title);
        itemName.setText(objekat.getTitle()); //postavljanje naziva filma u listi na MainActivity
        TextView genreView =  (TextView) newView.findViewById(R.id.genree);
        genreView.setText(objekat.getGenre());

        ImageView imageView = (ImageView) newView.findViewById(R.id.icon);
        /*
        if(objekat.getGenre().equalsIgnoreCase("crime"))
            imageView.setImageResource(R.drawable.crime);
        else if(objekat.getGenre().equalsIgnoreCase("animation"))
            imageView.setImageResource(R.drawable.animation);
        else  if(objekat.getGenre().equalsIgnoreCase("comedy"))
            imageView.setImageResource(R.drawable.comedy);
        else if(objekat.getGenre().equalsIgnoreCase("drama"))
            imageView.setImageResource(R.drawable.drama);
        else if(objekat.getGenre().equalsIgnoreCase("fantasy"))
            imageView.setImageResource(R.drawable.fantasy);
        else if(objekat.getGenre().equalsIgnoreCase("horror"))
            imageView.setImageResource(R.drawable.horror);
        else if(objekat.getGenre().equalsIgnoreCase("romance"))
            imageView.setImageResource(R.drawable.romantic);
        else if(objekat.getGenre().equalsIgnoreCase("romantic"))
            imageView.setImageResource(R.drawable.romantic);
        else if(objekat.getGenre().equalsIgnoreCase("thriller"))
            imageView.setImageResource(R.drawable.thriller);
        else if(objekat.getGenre().equalsIgnoreCase("action"))
            imageView.setImageResource(R.drawable.action);
        else if(objekat.getGenre().equalsIgnoreCase("superhero"))
            imageView.setImageResource(R.drawable.superhero);
        */
        Glide.with(getContext())
                .load(posterPath+objekat.getPosterPath())
                .centerCrop()
                .placeholder(R.drawable.movie)
                .error(R.drawable.movie)
                .fallback(R.drawable.movie)
                .into(imageView);

        return newView;
    }

    public Movie getMovie(int position) {
        return getItem(position);
    }
    public void setMovies(ArrayList<Movie> movies) {
        this.addAll(movies);
    }
}
