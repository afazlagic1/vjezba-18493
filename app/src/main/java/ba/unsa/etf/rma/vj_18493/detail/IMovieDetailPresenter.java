package ba.unsa.etf.rma.vj_18493.detail;

import android.os.Parcelable;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18493.data.Movie;

public interface IMovieDetailPresenter {

    void create(String title, String overview, String releaseDate, String genre, String homepage, ArrayList<String> actors, int id, String posterPath);
    void setMovie(Parcelable movie);
    void setID(int id);
    Movie getMovie();
}
