package ba.unsa.etf.rma.vj_18493.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18493.data.Movie;

public interface IMovieListInteractor {
    ArrayList<Movie> getMovies();
}
