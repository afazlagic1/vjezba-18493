package ba.unsa.etf.rma.vj_18493.detail;

import android.content.Context;
import android.os.Parcelable;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18493.data.Movie;

public class MovieDetailPresenter implements IMovieDetailPresenter, MovieDetailInteractor.onMovieFound {

    private Context context;
    private IMovieDetailView iMovieDetailView;
    private Movie movie;


    public MovieDetailPresenter(IMovieDetailView iMovieDetailView, Context context) {
        this.context    = context;
        this.iMovieDetailView = iMovieDetailView;
    }

    public MovieDetailPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void create(String title, String overview, String releaseDate, String genre, String homepage, ArrayList<String> actors, int id, String posterPath) {
        this.movie = new Movie(title,overview,releaseDate,homepage,genre,actors, id, posterPath);
    }

    @Override
    public void setMovie(Parcelable movie) {
        this.movie = (Movie)movie;
    }

    @Override
    public void setID(int id) {
        new MovieDetailInteractor(this).execute(String.valueOf(id));
    }

    @Override
    public Movie getMovie() {
        return movie;
    }

    @Override
    public void onFound(Movie movie) {
        iMovieDetailView.setMovie(movie);
    }
}
