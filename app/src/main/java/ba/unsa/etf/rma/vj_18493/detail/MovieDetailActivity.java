package ba.unsa.etf.rma.vj_18493.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import ba.unsa.etf.rma.vj_18493.R;

public class MovieDetailActivity extends AppCompatActivity {
    private TextView textTitle;
    private TextView textGenre;
    private TextView textReleaseDate;
    private TextView textHomepage;
    private TextView textOverview;
    private ListView listOfActors;
    private MovieDetailPresenter presenter;

    public MovieDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new MovieDetailPresenter(this);
        }
        return presenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_movie);

        textTitle = (TextView) findViewById(R.id.textTitle);
        textGenre = (TextView) findViewById(R.id.textGenre);
        textHomepage = (TextView) findViewById(R.id.textHomepage);
        textOverview = (TextView) findViewById(R.id.textOverview);
        textReleaseDate = (TextView) findViewById(R.id.textReleaseDate);
        listOfActors = (ListView) findViewById(R.id.actors);

        Intent incomingIntent = getIntent();
        String incomingString = incomingIntent.getStringExtra("movie");
        String incomingString2 = incomingIntent.getStringExtra("overview");
        String incomingString3 = incomingIntent.getStringExtra("genre");
        String incomingString4 = incomingIntent.getStringExtra("releaseDate");
        String incomingString5 = incomingIntent.getStringExtra("homepage");
        textTitle.setText(incomingString);
        textOverview.setText(incomingString2);
        textGenre.setText(incomingString3);
        textReleaseDate.setText(incomingString4);
        textHomepage.setText(incomingString5);

        /*TextView firstEditText1 = findViewById(R.id.firstEditText);
         Intent implicitniIntent = getIntent();
         firstEditText1.setText(implicitniIntent.getData().toString());*/
        textHomepage.setOnClickListener(homepageOnClickListener);
        textTitle.setOnClickListener(titleOnClickListener);
    }
    private View.OnClickListener homepageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String url = getPresenter().getMovie().getHomepage();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private View.OnClickListener titleOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_SEARCH);
            intent.setPackage("com.google.android.youtube");
            intent.putExtra("query", getPresenter().getMovie().getTitle() + " trailer" );
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private View.OnClickListener shareOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT,getPresenter().getMovie().getOverview());
            intent.setType("text/plain");
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };
}
