package ba.unsa.etf.rma.vj_18493;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import ba.unsa.etf.rma.vj_18493.data.Movie;

import static ba.unsa.etf.rma.vj_18493.MovieDBOpenHelper.MOVIE_GENRE;
import static ba.unsa.etf.rma.vj_18493.MovieDBOpenHelper.MOVIE_ID;
import static ba.unsa.etf.rma.vj_18493.MovieDBOpenHelper.MOVIE_TITLE;

public class MovieResourceCursorAdapter extends ResourceCursorAdapter {

    public MovieResourceCursorAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView itemName = (TextView) view.findViewById(R.id.title);
        TextView genreView =  (TextView) view.findViewById(R.id.genree);
        ImageView imageView = (ImageView) view.findViewById(R.id.icon);
        String[] koloneRezulat = new String[]{ MOVIE_ID, MOVIE_TITLE, MOVIE_GENRE};
        String title = cursor.getString(cursor.getColumnIndexOrThrow(MOVIE_TITLE));
        String genre = cursor.getString(cursor.getColumnIndexOrThrow(MOVIE_GENRE));
        itemName.setText(title);
        genreView.setText(genre);
    }
}
